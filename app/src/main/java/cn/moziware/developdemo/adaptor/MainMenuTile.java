
package cn.moziware.developdemo.adaptor;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.moziware.developdemo.R;

/**
 * Class which represents a clickable time to start an example
 */
public class MainMenuTile extends LinearLayout implements View.OnClickListener {
    private final String mLaunchClass;

    public MainMenuTile(Context context, int commandResID, int imageResID, String launchClass) {
        super(context);

        mLaunchClass = launchClass;

        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.command_square, this, true);

        setImage(imageResID);
        setSmallText(commandResID);
        setOnClickListener(this);
    }

    public void setImage(int resID) {
        ImageView iv = (ImageView) findViewById(R.id.imageView);
        Drawable drawable = getResources().getDrawable(resID, getContext().getTheme());
        iv.setImageDrawable(drawable);
    }

    public void setSmallText(int resID) {
        TextView tv = (TextView) findViewById(R.id.smallTextView);
        String text = getResources().getString(resID);
        tv.setText(text);
    }

    @Override
    public void onClick(View view) {
        String packageName = getContext().getPackageName();

        Intent intent = new Intent();
        intent.setComponent(new ComponentName(packageName, packageName + "." + mLaunchClass));
        getContext().startActivity(intent);
    }
}
