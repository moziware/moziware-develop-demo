
package cn.moziware.developdemo.helper;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Utils {
    public static File copyFromAssetsToExternal(Context context, String filename, String destinationFolder)
            throws IOException {

        InputStream inputStream = context.getAssets().open(filename);

        File outputFile = new File(context.getExternalFilesDir(destinationFolder), filename);
        OutputStream outputStream = new FileOutputStream(outputFile);

        byte[] buffer = new byte[1024];
        int read;
        while ((read = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, read);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();

        return outputFile;
    }
}
