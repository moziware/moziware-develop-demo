package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to accept dictation input from the user on a cimo device
 */
public class KeyboardActivity extends Activity {

    // Request code identifying dictation events
    private static final int DICTATION_REQUEST_CODE = 1;
    // Dictation intent action
    private final static String ACTION_DICTATION = "com.realwear.keyboard.intent.action.DICTATION";
    private EditText mTextField1;
    private EditText mTextField2;
    private EditText mTextField3;
    private EditText mTextField4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.keyboard_main);

        mTextField1 = findViewById(R.id.dictationField1);
        mTextField2 = findViewById(R.id.dictationField2);
        mTextField3 = findViewById(R.id.dictationField3);
        mTextField4 = findViewById(R.id.dictationField4);
    }

    /**
     * Listener for when a the launch keyboard button is clicked
     *
     */
    public void onLaunchKeyboard(View view) {
        mTextField1.setFocusable(true);
        mTextField1.setFocusableInTouchMode(true);
        mTextField1.requestFocus();

        InputMethodManager imm =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mTextField1, InputMethodManager.SHOW_FORCED);
    }

    public void onLaunchDictation1(View view) {
        mTextField2.setFocusable(true);
        mTextField2.setFocusableInTouchMode(true);
        mTextField2.requestFocus();

        InputMethodManager imm =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mTextField2, InputMethodManager.SHOW_FORCED);
    }

    /**
     * todo
     * Listener for when a the launch dictation button is clicked
     *
     */
    public void onLaunchDictation2(View view) {
        Intent intent = new Intent(ACTION_DICTATION);
        try {
            startActivityForResult(intent, DICTATION_REQUEST_CODE);
        }catch (Exception ignored){
            Toast.makeText(this, R.string.moziware_develop_text46, Toast.LENGTH_LONG).show();
        }
    }

    public void onLaunchBarcode(View view) {
        mTextField3.setFocusable(true);
        mTextField3.setFocusableInTouchMode(true);
        mTextField3.requestFocus();

        InputMethodManager imm =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mTextField3, InputMethodManager.SHOW_FORCED);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == DICTATION_REQUEST_CODE) {
            String result = "[Error]";
            if (data != null) {
                result = data.getStringExtra("result");
            }
            mTextField4.setText(result);
        }
    }
}
