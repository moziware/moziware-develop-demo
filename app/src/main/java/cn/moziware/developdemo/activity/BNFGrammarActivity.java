package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to register BNF grammar on a CIMO device.
 */
public class BNFGrammarActivity extends Activity {
    // The action that CIMO will use for broadcasting when a voice command is spoken.
    private static final String ACTION_SPEECH_EVENT =
            "com.realwear.wearhf.intent.action.SPEECH_EVENT";

    TextView year;
    TextView month;
    TextView day;
    TextView hour;
    TextView minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.bnf_activity);
        init();
    }

    void init() {
        year = findViewById(R.id.year);
        month = findViewById(R.id.month);
        day = findViewById(R.id.day);
        hour = findViewById(R.id.hour);
        minute = findViewById(R.id.minute);
        String DATE_AND_TIME_BNF = "#BNF+EM V2.0;" +
                "!grammar DateAndTime;\n" +
                "!start <DateAndTime>;\n" +
                "<DateAndTime>:<global_commands>|!optional(<" + year() + ">" + year() + ") !optional(<" + month() + ">" + month() + ") !optional(<" + day() + ">" + day() + ") !optional(<" + hour() + ">" + hour() + ") !optional(<" + minute() + ">" + minute() + ");\n" +
                "<" + year() + ">:1970|1971|1972|1973|1974|1975|1976|1977|1978|1979|1980|1981|1982|1983|1984|1985|1986|1987|1988|1989|1990|1991|1992|1993|1994|1995|1996|1997|1998|1999|2000|2001|2002|2003|2004|2005|2006|2007|2008|2009|2010|2011|2012|2013|2014|2015|2016|2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031|2032|2033|2034|2035|2036|2037|2038|2039|2040|2041|2042|2043|2044|2045|2046|2047|2048|2049;\n" +
                "<" + month() + ">:1|2|3|4|5|6|7|8|9|10|11|12;\n" +
                "<" + day() + ">:1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31;\n" +
                "<" + hour() + ">:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23;\n" +
                "<" + minute() + ">:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59;";
        // Set the BNF for this screen.
        // We are adding the BNF string to the TextView in the layout, but they can be added
        // to any view.
        // The broadcast receiver will get the result when the voice command is spoken.
        // The BNF string to register with
        findViewById(R.id.bnfDescription).setContentDescription("hf_override:" + DATE_AND_TIME_BNF);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(asrBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(asrBroadcastReceiver, new IntentFilter(ACTION_SPEECH_EVENT));
    }

    /**
     * Broadcast receiver for being notified when speech events occur
     */
    private final BroadcastReceiver asrBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action != null && action.equals(ACTION_SPEECH_EVENT)) {
                final String asrCommand = intent.getStringExtra("command");
                Toast.makeText(getBaseContext(), asrCommand, Toast.LENGTH_SHORT).show();
                setTextView(asrCommand);
            }
        }
    };

    //set date and time as where you receive from broadcast
    public void setTextView(String asrCommand) {
        if (!TextUtils.isEmpty(asrCommand)) {
            String[] asrWord = asrCommand.split(" ");
            if (asrCommand.contains(year())) {
                int num = num(asrWord, year());
                if (num > 0)
                    year.setText(getResources().getString(R.string.moziware_develop_text31, asrWord[num - 1]));
            }
            if (asrCommand.contains(month())) {
                int num = num(asrWord, month());
                if (num > 0)
                    month.setText(getResources().getString(R.string.moziware_develop_text32, asrWord[num - 1]));
            }
            if (asrCommand.contains(day())) {
                int num = num(asrWord, day());
                if (num > 0)
                    day.setText(getResources().getString(R.string.moziware_develop_text33, asrWord[num - 1]));
            }
            if (asrCommand.contains(hour())) {
                int num = num(asrWord, hour());
                if (num > 0)
                    hour.setText(getResources().getString(R.string.moziware_develop_text34, asrWord[num - 1]));
            }
            if (asrCommand.contains(minute())) {
                int num = num(asrWord, minute());
                if (num > 0)
                    minute.setText(getResources().getString(R.string.moziware_develop_text35, asrWord[num - 1]));
            }
        }
    }

    //The position of ruleName in the specified asrWord array
    public int num(String[] asrWord, String ruleName) {
        for (int i = 0; i < asrWord.length; i++) {
            if (ruleName.equals(asrWord[i])) {
                return i;
            }
        }
        return -1;
    }

    public String year() {
        return getString(R.string.moziware_develop_text36);
    }

    public String month() {
        return getString(R.string.moziware_develop_text37);
    }

    public String day() {
        return getString(R.string.moziware_develop_text38);
    }

    public String hour() {
        return getString(R.string.moziware_develop_text39);
    }

    public String minute() {
        return getString(R.string.moziware_develop_text40);
    }

    public void onClick(View view){
        Toast.makeText(getBaseContext(), "点了", Toast.LENGTH_SHORT).show();
    }

}
