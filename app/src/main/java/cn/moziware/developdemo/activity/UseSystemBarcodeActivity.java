package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to scan a barcode using moziware device
 */
public class UseSystemBarcodeActivity extends Activity {

    private static final int BARCODE_REQUEST_CODE = 1234;
    private static final String ACTION_SCAN_BARCODE =
            "cn.moziware.barcodescanner.action.SCAN_BARCODE";
    private static final String EXTRA_RESULT = "cn.moziware.barcodescanner.extra.RESULT";
    private TextView mBarcodeResultsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.use_system_barcodescanner);
        mBarcodeResultsView = (TextView) findViewById(R.id.barcode_textview);
    }

    public void onLaunchBarcode(View view) {
        Intent intent = new Intent(ACTION_SCAN_BARCODE);
        startActivityForResult(intent, BARCODE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == BARCODE_REQUEST_CODE) {
            String result = getString(R.string.moziware_develop_text6);
            if (data != null) {
                result = data.getStringExtra(EXTRA_RESULT);
                Toast.makeText(getApplicationContext(), R.string.moziware_develop_text7, Toast.LENGTH_SHORT).show();
            }
            mBarcodeResultsView.setText(result);
        }
    }
}