package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import cn.moziware.developdemo.R;

/**
 * Activity that shows how to release the microphones on a CIMO device
 */
public class ReleaseMicrophoneActivity extends Activity {

    // request to release microphone
    private static final String ACTION_RELEASE_MIC =
            "com.realwear.wearhf.intent.action.RELEASE_MIC";
    // request to resume microphone
    private static final String ACTION_MIC_RELEASED =
            "com.realwear.wearhf.intent.action.MIC_RELEASED";

    // Identifiers for the settings which allow modification of the on screen message shown when the microphone has been released
    private static final String EXTRA_MUTE_TEXT = "com.realwear.wearhf.intent.extra.MUTE_TEXT";
    // Not allow to show message on screen when the microphone has been released
    private static final String EXTRA_HIDE_TEXT = "com.realwear.wearhf.intent.extra.HIDE_TEXT";
    private static final String EXTRA_SOURCE_PACKAGE =
            "com.realwear.wearhf.intent.extra.SOURCE_PACKAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.release_microphone);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Register ACTION_RELEASE_MIC and ACTION_MIC_RELEASED broadcast when activity resume
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_RELEASE_MIC);
        filter.addAction(ACTION_MIC_RELEASED);
        registerReceiver(micBroadcastReceiver, filter);
    }
    //
    @Override
    protected void onPause() {
        super.onPause();
        // Make sure we resume the microphone on pause so we don't go into the background with it still blocking ASR
        Intent intent = new Intent(ACTION_MIC_RELEASED);
        intent.putExtra(EXTRA_SOURCE_PACKAGE, getPackageName());
        sendBroadcast(intent);
        unregisterReceiver(micBroadcastReceiver);
    }

    // Listener for when a the release microphone button is clicked
    public void onReleaseMicrophoneWithDefaultText(View view) {
        Intent intent = new Intent(ACTION_RELEASE_MIC);
        intent.putExtra(EXTRA_SOURCE_PACKAGE, getPackageName());
        // By default system will put up big display to say that
        // 'Use the left/right button to reactivate the microphone'
        // If we don't want any text on screen, set the HIDE_TEXT extra to true
        intent.putExtra(EXTRA_HIDE_TEXT, false);
        // If we want to show text but change the message, use the MUTE_TEXT extra
        //intent.putExtra(EXTRA_MUTE_TEXT, "Mic is Off. Press Left/Right Button to Resume");

        sendBroadcast(intent);
    }

    public void onReleaseMicrophoneWithMyText(View view) {
        Intent intent = new Intent(ACTION_RELEASE_MIC);
        intent.putExtra(EXTRA_SOURCE_PACKAGE, getPackageName());
        // By default system will put up big display to say that
        // 'Use the left/right button to reactivate the microphone'
        // If we don't want any text on screen, set the HIDE_TEXT extra to true
        intent.putExtra(EXTRA_HIDE_TEXT, false);
        // If we want to show text but change the message, use the MUTE_TEXT extra
        intent.putExtra(EXTRA_MUTE_TEXT, "Mic is Off. Press Left/Right Button to Resume");

        sendBroadcast(intent);
    }

    public void onReleaseMicrophoneWithoutText(View view) {
        Intent intent = new Intent(ACTION_RELEASE_MIC);
        intent.putExtra(EXTRA_SOURCE_PACKAGE, getPackageName());
        // By default system will put up big display to say that
        // 'Use the left/right button to reactivate the microphone'
        // If we don't want any text on screen, set the HIDE_TEXT extra to true
        intent.putExtra(EXTRA_HIDE_TEXT, true);
        // If we want to show text but change the message, use the MUTE_TEXT extra
        //intent.putExtra(EXTRA_MUTE_TEXT, "Mic is Off. Press Left/Right Button to Resume");

        sendBroadcast(intent);
    }

    private final BroadcastReceiver micBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String sourcePackage = intent.getStringExtra(EXTRA_SOURCE_PACKAGE);
            if (sourcePackage != null) {
                if (sourcePackage.equals(getApplicationContext().getPackageName())) {
                    return;
                }
            }
            if (intent.getAction().equals(ACTION_MIC_RELEASED)) {
                // The system has now released the microphones
                Toast.makeText(context, R.string.moziware_develop_text30, Toast.LENGTH_LONG).show();
            } else if (intent.getAction().equals(ACTION_RELEASE_MIC)) {
                // The system wants to access the microphone so release and inform the system
                Intent newIntent = new Intent(ACTION_MIC_RELEASED);
                intent.putExtra(EXTRA_SOURCE_PACKAGE, getApplicationContext().getPackageName());
                sendBroadcast(newIntent);
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case 184:
            case 186:
                Intent intent = new Intent(ACTION_MIC_RELEASED);
                intent.putExtra(EXTRA_SOURCE_PACKAGE, getPackageName());
                sendBroadcast(intent);
                break;
        }
        return super.onKeyDown(keyCode, event);
    }
}
