package cn.moziware.developdemo.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import cn.moziware.developdemo.R;

public class NoiseCancellationActivity extends AppCompatActivity {
    private static final String TAG = "NoiseCancellationActivity";

    private static final String PACKAGE_NAME = "cn.moziware.wearable";
    private static final String ACTION_GET_NOISE_CANCELLATION = "cn.moziware.wearable.action.GET_NOISE_CANCELLATION";
    private static final String ACTION_SET_NOISE_CANCELLATION_MODE = "cn.moziware.wearable.action.SET_NOISE_CANCELLATION_MODE";

    private static final String EXTRA_MODE = "cn.moziware.wearable.extra.MODE";

    private static final String MODE_NONE = "none";
    private static final String MODE_DUALMIC = "dualmic";

    TextView nc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.noise_cancellation);
        nc = findViewById(R.id.mode);
        getStatus();
    }

    public void onNoneClick(View view) {
        disableNoiseCancellation();
    }

    public void onDualmicClick(View view) {
        enableNoiseCancellation();
    }

    public void onRetrieveInfo(View view) {
        getNoiseCancellation();
    }


    private void enableNoiseCancellation() {
        final Intent intent = new Intent(ACTION_SET_NOISE_CANCELLATION_MODE);
        intent.setPackage(PACKAGE_NAME);
        intent.putExtra(EXTRA_MODE, MODE_DUALMIC);
        sendBroadcast(intent);
        getStatus();
    }

    private void disableNoiseCancellation() {
        final Intent intent = new Intent(ACTION_SET_NOISE_CANCELLATION_MODE);
        intent.setPackage(PACKAGE_NAME);
        intent.putExtra(EXTRA_MODE, MODE_NONE);
        sendBroadcast(intent);
        getStatus();
    }

    private void getNoiseCancellation() {
        final Intent intent = new Intent(ACTION_GET_NOISE_CANCELLATION);
        intent.setPackage(PACKAGE_NAME);

        sendOrderedBroadcast(intent, null, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (getResultCode() != 1) {
                    Log.e(TAG, "Failed to get result.");
                    return;
                }
                final Bundle extras = getResultExtras(false);
                if (extras == null) {
                    Log.e(TAG, "Failed to get extras.");
                    return;
                }
                final String mode = extras.getString(EXTRA_MODE);
                nc.setText(getString(R.string.moziware_develop_text47, mode));
                Toast.makeText(
                        context,
                        "Noise cancellation: mode=" + mode,
                        Toast.LENGTH_SHORT
                ).show();
            }
        }, null, 0, null, null);
    }
    
    public void getStatus() {
        new Thread(() -> {
            try {
                Thread.sleep(500);
                getNoiseCancellation();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}