package cn.moziware.developdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;

import cn.moziware.developdemo.R;
import cn.moziware.developdemo.helper.Utils;

/**
 * Activity that shows how open a video on moziware device
 */
public class UseSystemMediaPlayerActivity extends Activity {

    private File mSampleFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.use_system_media_player);

        try {
            String sampleFileName = "moziware_cimo_introduction.mp4";
            String sampleFolderName = "Movies";
            mSampleFile = Utils.copyFromAssetsToExternal(this, sampleFileName, sampleFolderName);
        } catch (IOException ex) {
            Toast.makeText(this, "Failed to Copy Sample File", Toast.LENGTH_LONG).show();
        }
    }

    public void onLaunchVideo(View view) {
        if (mSampleFile == null) {
            Toast.makeText(getApplicationContext(),"Failed to find sample file",Toast.LENGTH_LONG).show();
            return;
        }

        final Uri contentUri = FileProvider.getUriForFile(
                getApplicationContext(),
                getApplicationContext().getPackageName() + ".fileprovider",
                mSampleFile);

        final Intent viewIntent = new Intent(Intent.ACTION_VIEW);
        viewIntent.addCategory(Intent.CATEGORY_DEFAULT);
        viewIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        String sampleMimeType = "video/mp4";
        viewIntent.setDataAndType(contentUri, sampleMimeType);
        viewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(viewIntent);
    }
}
