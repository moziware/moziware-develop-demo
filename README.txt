English:

This source code is used to show how to develop applications on CIMO devices.
It mainly shows how to use various built-in APIs on CIMO devices, including some common Android functions.

1. How to call the camera to take photos or video and return the results

2. How to call the document viewer to view documents

3. How to call barcode scanner to scan QR code or barcode and return results

4. How to use the sound recording function

5. How to add voice commands to voice engine

6. How to call media player to play video

7. How to temporarily disable the system voice engine

8. How to register massive voice commands using BNF grammer

9. How to call the virtual keyboard of the system

10. Show some key information of CIMO equipment

11. How to control device noise cancellation feature 

中文:

此源代码用于演示如何在CIMO设备上开发应用程序。
它主要展示了如何在CIMO设备上使用各种内置API，包括一些常见的Android功能。

1. 如何调用系统相机拍照与录像

2. 如何调用系统文档查看器

3. 如何调用系统二维码扫描器

4. 如何使用系统的录音API

5. 如何添加临时语音命令到全局命令列表

6. 如何调用系统视频播放器

7. 如何暂时停用系统语音引擎

8. 如何使用 BNF 语法注册海量语音命令

9. 如何调用系统的虚拟键盘

10. 展示系统关键信息

11. 如何控制系统的降噪功能
